$(document).ready(function() {
	var utm_source = getUrlParameter('utm_source');
	var utm_medium = getUrlParameter('utm_medium');
	var utm_term = getUrlParameter('utm_term');
	var utm_campaign = getUrlParameter('utm_campaign');
	$('input[name=utm_source]').val(utm_source);
	$('input[name=utm_medium]').val(utm_medium);
	$('input[name=utm_term]').val(utm_term);
	$('input[name=utm_campaign]').val(utm_campaign);
});

// script to get utm
var getUrlParameter = function getUrlParameter(sParam)
{
  var sPageURL = decodeURIComponent(window.location.search.substring(1)),
    sURLVariables = sPageURL.split('&'),
    sParameterName,
    i;
  for (i = 0; i < sURLVariables.length; i++)
  {
    sParameterName = sURLVariables[i].split('=');
    if (sParameterName[0] === sParam)
    {
      return sParameterName[1] === undefined ? true : sParameterName[1];
    }
  }
};


function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for(var i=0; i<ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1);
			if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
	}
	return "";
}

function setCookie(cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays*24*60*60*1000));
	var expires = "expires="+d.toUTCString();
	document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/";
}


function clearF1Cookie() {
	setCookie("name","",-1);
	setCookie("email","",-1);
	setCookie("last1","",-1);
}
$(window).load(function() {
	$("input.name").val(getCookie("name"));
	$("input.email").val(getCookie("email"));
	$("input.phone").val(getCookie("phone"));
});

$('.btn-anchor').on('click', function(e) {
	e.preventDefault();
	anchorScroller(this, 1500);
});



$('.format').click(function(){
	$('.format').removeClass('active');
	$(this).addClass('active');
	$('.package').removeClass('inactive');
	$('.package').addClass('active');
	$('.combo-package').addClass('active');
	
	var fullPrice = $(this).attr('data-full');
	var newPrice = $(this).attr('data-now');
	var discount = $(this).attr('data-discount');
	var fullPriceBundle = $(this).attr('data-bundle-full');
	var newPriceBundle = $(this).attr('data-bundle-new');
	var discountBundle = $(this).attr('data-bundle-discount');
	var vpo = $(this).attr('data-vpo');
	var ksi = $(this).attr('data-ksi');
	var city = $(this).attr('data-city');
	var type = $(this).attr('data-type');
	var tagsVpo = $(this).attr('data-tags-vpo');
	var tagsKsi = $(this).attr('data-tags-ksi');
	var tagsBundle = $(this).attr('data-tags-bundle');

	
	$('.package .old-price span').html(fullPrice);
	$('.package .new-price span').html(newPrice);
	$('.combo-package .old-price span').html(fullPriceBundle);
	$('.combo-package .old-price span').html(newPriceBundle);
	$('.package .decor-txt2 span').html(discount);
	$('.combo-package .decor-txt2 span').html(discountBundle);
	$('.btn_pink').attr({'data-type': type, 'data-city': city, 'data-sum': newPrice });
	$('.btn_vpo').attr('data-tags', tagsVpo);
	$('.btn_ksi').attr('data-tags', tagsKsi);
	$('.btn_bundle').attr('data-tags', tagsBundle);
//	$('.left-package .date-txt2').html(vpo);
//	$('.closest-date1 .date-vpo').html(vpo);
//	$('.right-package .date-txt2').html(ksi);
//	$('.closest-date2 .date-ksi').html(ksi);
	
	//$('.sec3').css('margin-top', 'margin-top: -425px');
})

$(document).ready(function(){
	var btn = document.querySelector('.sec-scroll .scroll-center');

  window.onscroll = magic;

  function magic() {
      if (window.pageYOffset > 2220) {
      btn.style.opacity = '1';
    } else { btn.style.opacity = '0'; }
  	}
		
})

$(document).ready(function()
	{
		function showInfo(data, tabletop){
			var mkName = 'list1'; // tabname of googleSheet
			var kyivVPO = tabletop.sheets(mkName).all()[0].kyiv;
			var kyivKSI = tabletop.sheets(mkName).all()[1].kyiv;
			var odessaVPO = tabletop.sheets(mkName).all()[0].odessa;
			var odessaKSI = tabletop.sheets(mkName).all()[1].odessa;
			var moscowVPO = tabletop.sheets(mkName).all()[0].moscow;
			var moscowKSI = tabletop.sheets(mkName).all()[1].moscow;
			var almatiVPO = tabletop.sheets(mkName).all()[0].almati;
			var almatiKSI = tabletop.sheets(mkName).all()[1].almati;
			console.log(kyivVPO);
			console.log(kyivVPO);
			console.log(moscowVPO);
			console.log(almatiVPO);
			$('.city1').attr({'data-vpo': kyivVPO, 'data-ksi':kyivKSI});
			

		}
		setInterval(function()
		{
			var tabletop = Tabletop.init({
				key: '1TIJAuXs-fe8Pie4Z96CCkDkBjZX9D9wYHa7ddaAQlhE',
				callback: showInfo
			})
		}, 10000);
	});

$('.btn_pink').click(function(){
	//event.preventDefault();
	var city = $(this).attr('data-city');
	var type = $(this).attr('data-type');
	var sum = $(this).attr('data-sum');
	var tags = $(this).attr('data-tags');
	setCookie('wincity', city, 365);
	setCookie('winsum', sum, 365);
	setCookie('wintype', type, 365);
	setCookie('wintags', tags, 365);
	//var wincity = $.cookie('wincity');

})