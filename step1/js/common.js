$(document).ready(function() {
	var utm_source = getUrlParameter('utm_source');
	var utm_medium = getUrlParameter('utm_medium');
	var utm_term = getUrlParameter('utm_term');
	var utm_campaign = getUrlParameter('utm_campaign');
	$('input[name=utm_source]').val(utm_source);
	$('input[name=utm_medium]').val(utm_medium);
	$('input[name=utm_term]').val(utm_term);
	$('input[name=utm_campaign]').val(utm_campaign);
});

// script to get utm
var getUrlParameter = function getUrlParameter(sParam)
{
  var sPageURL = decodeURIComponent(window.location.search.substring(1)),
    sURLVariables = sPageURL.split('&'),
    sParameterName,
    i;
  for (i = 0; i < sURLVariables.length; i++)
  {
    sParameterName = sURLVariables[i].split('=');
    if (sParameterName[0] === sParam)
    {
      return sParameterName[1] === undefined ? true : sParameterName[1];
    }
  }
};


function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for(var i=0; i<ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1);
			if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
	}
	return "";
}

function setCookie(cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays*24*60*60*1000));
	var expires = "expires="+d.toUTCString();
	document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/";
}


function clearF1Cookie() {
	setCookie("name","",-1);
	setCookie("email","",-1);
	setCookie("last1","",-1);
}
$(window).load(function() {
	$("input.name").val(getCookie("name"));
	$("input.email").val(getCookie("email"));
	$("input.phone").val(getCookie("phone"));
	$("input.city").val(getCookie("wincity"));
	$("input.sum").val(getCookie("winsum"));
	$("input.type").val(getCookie("wintype"));
	$("input.tags").val(getCookie("wintags"));
});



$('.btn-anchor').on('click', function(e) {
	e.preventDefault();
	anchorScroller(this, 1500);
});



// $('.step .next-step').on('click', function(){
// 	var thisStep = $(this).closest('.step');
// 	var target = $(this).data('target');
// 	var done = $(this).closest('.step').data('done');
// 	if (thisStep.hasClass('.step')){
// 		thisStep.find('.name').focus();
// 		thisStep.find('.name2').focus();
// 		thisStep.find('.email').focus();
// 		thisStep.find('.phone').focus();
// 		thisStep.find('.phone2').focus();
// 		thisStep.find('.next-step').focus();
// 		if ($('.not_error').length >= 3) {
// 			$('.step.active').removeClass('active');
// 			$('.step.' + target).addClass('active');
// 			$('.form-steps .'+target).addClass('active');
// 			$('.form-steps .'+done).addClass('done');
// 			$('.line.' +done).addClass('done');
// 			$('.line.' +target).addClass('active');
// 		}
// 		else {
// 			form.find('input.error').first().focus();
// 		}
// 	}
// 	else {

// 		$('.step.active').removeClass('active');
// 		$('.step.' + target).addClass('active');
// 		$('.form-steps .'+target).addClass('active');
// 		$('.form-steps .'+done).addClass('done');
// 		$('.line.' +done).addClass('done');
// 		$('.line.' +target).addClass('active');
// 	}


// })

// $('.step .back').on('click', function(){
// 	var target = $(this).data('target');
// 	var done = $(this).closest('.step').data('done');
// 	$('.step.active').removeClass('active');
// 	$('.step.' + target).addClass('active');
// 	$('.form-steps .'+target).addClass('active').removeClass('done');
// 	$('.form-steps .'+done).removeClass('active');
// 	$('.line .'+done).removeClass('active');
// 	$('.line .'+target).removeClass('active').removeClass('done');
// 	$(this).find('.step-number').removeClass('active');
// });


// $(document).ready(function(){ 
// var element = document.getElementById('timer'); 
// var dateEnd; console.log($.cookie("timer")); 
// 	if ($.cookie("timer")) { dateEnd = $.cookie("timer"); } 
// 	else { dateEnd = new Date(); dateEnd.setMinutes(dateEnd.getMinutes()+9); 
// 	$.cookie("timer", dateEnd, {expires: null}); dateEnd = 'in 9 minutes'; }; 
// 	var options = { 
// 	due: dateEnd, l
// 	ayout: 'label-small', 
// 	face: 'slot', 
// 	format: 'h,m,s,ms', 
// 	labelsGours: "Часы,Часов", 
// 	labelMinutes: "Минуты,Минут", 
// 	labelsSeconds: "Секунды,Секунд", 
// 	labelsMilliseconds: "Милисекунд" }; 
	// $('#timer').soon().create(options); Soon.create(element, options); });